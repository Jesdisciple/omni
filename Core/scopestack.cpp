#include <iostream>

#include "scopestack.h"

using namespace Omni;

ScopeStack::ScopeStack()
{
	stack.push_back(global);
}

ScopeStack::~ScopeStack()
{
	stack = std::vector<Namespace>();
}

const ScopeStack & ScopeStack::operator =(const ScopeStack & other)
{
	stack = other.stack;
	return other;
}

bool ScopeStack::findVariable(std::string name, Namespace *& ns, Namespace::iterator & it)
{
	for(auto sentry = stack.rbegin(); sentry != stack.rend(); sentry++)
	{
		if(sentry->findVariable(name, it))
		{
			ns = &*sentry;
			return true;
		}
	}
	return false;
}

void ScopeStack::declareVariable(Ref<Type> type, std::string name)
{
	stack.back().declareVariable(type, name);
}

Ref<Value> ScopeStack::resolveVariable(std::string name)
{
	Namespace * ns = nullptr;
	Namespace::iterator it;

	if(findVariable(name, ns, it))
	{
		return ns->resolveVariable(it);
	}
	return nullptr;
}

bool ScopeStack::tryResolveVariable(std::string name, Ref<Value>& out_value)
{
	Namespace * ns = nullptr;
	Namespace::iterator it;

	if (findVariable(name, ns, it))
	{
		out_value = ns->resolveVariable(it);
		return true;
	}
	out_value = nullptr;
	return false;
}

void ScopeStack::assignVariable(std::string name, Ref<Value> value)
{
	Namespace * ns = nullptr;
	Namespace::iterator it;

	if(findVariable(name, ns, it))
	{
		ns->assignVariable(it, value);
	}
}

Ref<Type> ScopeStack::getTypeOfVariable(std::string name)
{
	Namespace * ns = nullptr;
	Namespace::iterator it;

	if(findVariable(name, ns, it))
	{
		return ns->getTypeOfVariable(it);
	}
	return nullptr;
}

void ScopeStack::scope(Namespace & ns, std::function<void()> fn)
{
	stack.push_back(ns);

	fn();

	stack.pop_back();
}
