#include "identifier.h"

using namespace Omni;

Identifier::Identifier(Ref<Type> type)
	: Value(type)
{
}

void Identifier::init(std::string name)
{
	this->name = name;
}

std::string Identifier::getName() const
{
	return name;
}

Ref<Value> Identifier::copy()
{
	auto result = std::make_shared<Identifier>(getType());
	result->init(name);
	return result;
}

void Identifier::writeTo(std::ostream & stream) const
{
	stream << "<identifier " << name << ">";
}
