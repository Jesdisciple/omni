#pragma once

#include <string>

#include "../Common/ref.h"

#include "../Types/value.h"

namespace Omni
{
	class Identifier : public Value
	{
	private:
		friend class PrimitiveType;

		std::string name;

	public:
		Identifier(Ref<Type> type);

		void init(std::string name);

		std::string getName() const;

		virtual Ref<Value> copy() override;
		virtual void writeTo(std::ostream & stream) const override;
	};
}