#pragma once

#include "../Lexer/lexer.h"

namespace Omni
{
	class Parser
	{
	public:
		Parser(Lexer & lexer);
		~Parser();
	};
}