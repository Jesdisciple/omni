#include "lisp.h"

#include <stack>

using namespace Omni;

Lisp::Lisp()
{
	runtime = std::make_shared<Runtime>();

	numberToken = lexer.defineToken("number", "\\s*([-+]?\\d+(?:\\.\\d+)?)\\s*");
	lparenToken = lexer.defineToken("lparen", "\\s*(\\()\\s*");
	rparenToken = lexer.defineToken("rparen", "\\s*(\\))\\s*");
	identifierToken = lexer.defineToken("identifier", "\\s*([^ \t\n()0-9][^ \t\n()]*)\\s*");

	auto numberType = runtime->getNumberType();
	auto mathFunction = runtime->getFunctionType(numberType, "MathFunction", { numberType, numberType });
	mathFunction->create("+", {"a", "b"}, [this]()
	{
		return plus(runtime.get());
	});
	mathFunction->create("-", {"a", "b"}, [this]()
	{
		return minus(runtime.get());
	});
}

Ref<List> Lisp::parse(std::vector<Lexeme>& lex)
{
	Ref<Type> typeType = runtime->getTypeType();
	Ref<Type> numberType = runtime->getNumberType();
	Ref<Type> listType = runtime->getListType();
	Ref<Type> identifierType = PrimitiveType::createType<Identifier>(typeType, "identifier");

	std::stack<Ref<List>> stack;
	Ref<List> frame = runtime->list();
	stack.push(frame);

	for (auto lexeme : lex)
	{
		if (lexeme.getToken() == lparenToken)
		{
			//PRINT("BRANCH_START");
			frame = runtime->list();
			stack.push(frame);
		}
		else if (lexeme.getToken() == rparenToken)
		{
			//PRINT("BRANCH_END");
			Ref<List> temp = stack.top();

			stack.pop();
			if (stack.size() == 0)
			{
				// TODO: exception
			}

			frame = stack.top();
			frame->add(temp);
		}
		else if (lexeme.getToken() == numberToken)
		{
			//PRINT("NUMBER");
			frame->add(std::make_shared<Number>(numberType, std::stod(lexeme.getText())));
		}
		else if (lexeme.getToken() == identifierToken)
		{
			//PRINT("IDENTIFIER");
			auto result = std::make_shared<Identifier>(identifierType);
			result->init(lexeme.getText());
			frame->add(result);
		}
	}

	if (stack.size() != 1)
	{
		// TODO: exception
	}

	return frame;
}

Ref<List> Lisp::eval(std::string source)
{
	auto lexemes = lexer.scan(source);

	Ref<List> result;

	runtime->scope([this, &lexemes, &result]()
	{
		Ref<List> tree = parse(lexemes);

		tree = runtime->compile(tree);

		result = runtime->execute(tree);
	});

	return result;
}

Ref<Value> Lisp::plus(Runtime * runtime)
{
	double a, b;

	a = runtime->resolveVariable("a")->as<Number>()->getValue();
	b = runtime->resolveVariable("b")->as<Number>()->getValue();

	return runtime->number(a + b);
}

Ref<Value> Lisp::minus(Runtime * runtime)
{
	double a, b;

	a = runtime->resolveVariable("a")->as<Number>()->getValue();
	b = runtime->resolveVariable("b")->as<Number>()->getValue();

	return runtime->number(a - b);
}
