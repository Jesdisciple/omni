#ifndef VARIABLE_H
#define VARIABLE_H

#include <string>

#include "../Types/type.h"
#include "../Types/value.h"

namespace Omni
{
	class Variable
	{
	private:
		Ref<Type> type;
		std::string name;
		Ref<Value> value;

		std::shared_ptr<bool> exists = std::make_shared<bool>(true);

	public:
		Variable(const Variable & other) = delete;
		const Variable & operator =(const Variable &) = delete;

		Variable(Ref<Type> type, std::string name, Ref<Value> value = nullptr);
		~Variable();

		Ref<Type> getType() const;
		std::string getName() const;
		Ref<Value> getValue() const;
		std::shared_ptr<bool> getExists() const;
		void setValue(Ref<Value> value);
	};
}

#endif // VARIABLE_H
