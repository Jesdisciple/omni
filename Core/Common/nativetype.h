#pragma once

#include <memory>

#include "ref.h"

namespace Omni
{
	using NativeType = size_t;

	class EnableNativeType_helper
	{
	private:
		EnableNativeType_helper() = delete;

		static NativeType indexCount;

		template<typename T>
		friend NativeType typeOf();
	};

	template<typename T>
	NativeType typeOf()
	{
		static NativeType index = EnableNativeType_helper::indexCount++;
		return index;
	}

	template<typename T>
	NativeType typeOf(std::shared_ptr<const T> t)
	{
		return t->getNativeType();
	}

	template<typename T>
	NativeType typeOf(std::shared_ptr<T> t)
	{
		return t->getNativeType();
	}

	template<typename T>
	NativeType typeOf(std::unique_ptr<const T> t)
	{
		return t->getNativeType();
	}

	template<typename T>
	NativeType typeOf(std::unique_ptr<T> t)
	{
		return t->getNativeType();
	}

	template<typename T>
	NativeType typeOf(const T * t)
	{
		return t->getNativeType();
	}

	template<typename T>
	NativeType typeOf(T * t)
	{
		return t->getNativeType();
	}
}