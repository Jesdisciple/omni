#include <exception>
#include <iostream>
#include <sstream>

#include "ref.h"
#include "variable.h"

using namespace Omni;

Variable::Variable(Ref<Type> type, std::string name, Ref<Value> value)
	: type(type), name(name), value(value)
{
}

Variable::~Variable()
{
	type = nullptr;
	value = nullptr;
	*exists = false;
}

Ref<Type> Variable::getType() const
{
	return type;
}

std::string Variable::getName() const
{
	return name;
}

Ref<Value> Variable::getValue() const
{
	if(value == nullptr)
	{
		return nullptr;
	}
	return value->copy();
}

std::shared_ptr<bool> Variable::getExists() const
{
	return exists;
}

void Variable::setValue(Ref<Value> value)
{
	//std::cout << "variable type: " << *type << "\n";
	//std::cout << "value type: " << *value->getType() << "\n";

	if (!value->getType()->satisfies(type))
	{
		std::ostringstream ss{};
		ss << "type mismatch: " << getType()->getName() << " != " << value->getType()->getName();
		throw std::invalid_argument(ss.str());
	}
	this->value = std::move(value);
}
