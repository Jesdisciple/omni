#ifndef NAMESPACE_H
#define NAMESPACE_H

#include <vector>
#include <memory>
#include <map>
#include <unordered_map>

#include "variable.h"

namespace Omni
{
	class Namespace
	{
		private:
			using Map = std::map<std::string, std::unique_ptr<Variable>>;
			Map members;

			static Map copy(const Map & m);

			Namespace(const Map & members);

		public:
			using iterator = Map::iterator;

			Namespace();
			Namespace(const std::vector<Variable> & variables);
			Namespace(const std::vector<Variable> & variables, std::vector<Ref<Value>> values);
			Namespace(const Namespace & other);
			~Namespace();
			const Namespace & operator =(const Namespace & other);

			bool findVariable(std::string name, iterator & it);
			void declareVariable(Ref<Type> type, std::string name);

			void assignVariable(iterator it, Ref<Value> value);
			void assignVariable(std::string name, Ref<Value> value);

			Ref<Value> resolveVariable_impl(iterator it);
			Ref<Value> resolveVariable(iterator it);
			Ref<Value> resolveVariable(std::string name);
			bool tryResolveVariable(std::string name, Ref<Value> & out_value);

			Ref<Type> getTypeOfVariable_impl(iterator it);
			Ref<Type> getTypeOfVariable(iterator it);
			Ref<Type> getTypeOfVariable(std::string name);
			bool tryGetTypeOfVariable(std::string name, Ref<Type> out_type);
	};
}

#endif // NAMESPACE_H
