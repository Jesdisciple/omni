#include <utility>
#include <iostream>

#include "../Common/namespace.h"

using namespace Omni;

Namespace::Namespace(const Map & members)
	: members(copy(members))
{
}

Namespace::Namespace()
{
}

Namespace::Namespace(const std::vector<Variable> & variables)
	: Namespace()
{
	for(size_t i = 0; i < variables.size(); i++)
	{
		std::string name = variables[i].getName();
		declareVariable(variables[i].getType(), name);
	}
}

Namespace::Namespace(const std::vector<Variable> & variables, std::vector<Ref<Value>> values)
	: Namespace()
{
	if(variables.size() < values.size())
	{
		// TODO: error
		return;
	}

	for(size_t i = 0; i < variables.size(); i++)
	{
		std::string name = variables[i].getName();
		declareVariable(variables[i].getType(), name);
		assignVariable(name, values[i]->copy());
	}
}

Namespace::Namespace(const Namespace & other)
	: Namespace(other.members)
{
}

Namespace::~Namespace()
{
	members = Map();
}

const Namespace & Namespace::operator =(const Namespace & other)
{
	members = copy(other.members);
	return other;
}

Namespace::Map Namespace::copy(const Map & m)
{
	Map result;
	for(auto& pair : m)
	{
		std::string name = pair.first;

		const std::unique_ptr<Variable> & var = pair.second;

		Ref<Type> type = var->getType();

		const Ref<Value> & value = var->getValue();
		Ref<Value> newValue = nullptr;
		if(value != nullptr)
		{
			newValue = value->copy();
		}

		result[name] = std::unique_ptr<Variable>(new Variable(type, name, std::move(newValue)));
	}
	return result;
}

bool Namespace::findVariable(std::string name, iterator & it)
{
	it = members.find(name);
	return it != members.end();
}

void Namespace::declareVariable(Ref<Type> type, std::string name)
{
	auto var = std::unique_ptr<Variable>(new Variable(type, name));
	members[name] = std::move(var);
}

#pragma region assignVariable
void Namespace::assignVariable(iterator it, Ref<Value> value)
{
	if(it == members.end())
	{
		// TODO: error
		return;
	}

	//std::cout << "value type: " << *value->getType() << "\n";
	it->second->setValue(std::move(value));
}

void Namespace::assignVariable(std::string name, Ref<Value> value)
{
	assignVariable(members.find(name), value);
}
#pragma endregion

#pragma region resolveVariable
Ref<Value> Namespace::resolveVariable_impl(iterator it)
{
	if (it->second == nullptr)
	{
		// TODO: error
		return nullptr;
	}
	return it->second->getValue();
}

Ref<Value> Namespace::resolveVariable(iterator it)
{
	if (it == members.end())
	{
		// TODO: error
		return nullptr;
	}
	return resolveVariable_impl(it);
}

Ref<Value> Namespace::resolveVariable(std::string name)
{
	return resolveVariable(members.find(name));
}

bool Namespace::tryResolveVariable(std::string name, Ref<Value> & out_value)
{
	auto it = members.find(name);
	if (it != members.end())
	{
		out_value = resolveVariable_impl(it);
		return true;
	}
	out_value = nullptr;
	return false;
}
#pragma endregion

#pragma region getTypeOfVariable
Ref<Type> Namespace::getTypeOfVariable_impl(iterator it)
{
	return it->second->getType();
}

Ref<Type> Namespace::getTypeOfVariable(iterator it)
{
	if (it == members.end())
	{
		// TODO: error
		return nullptr;
	}
	return it->second->getType();
}

Ref<Type> Namespace::getTypeOfVariable(std::string name)
{
	return getTypeOfVariable(members.find(name));
}

bool Namespace::tryGetTypeOfVariable(std::string name, Ref<Type> out_type)
{
	auto it = members.find(name);
	if (it != members.end())
	{
		out_type = getTypeOfVariable_impl(it);
		return true;
	}
	out_type = nullptr;
	return false;
}
#pragma endregion