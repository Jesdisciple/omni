#ifndef REF_H
#define REF_H

#include <memory>
#include <ostream>

namespace Omni
{
	template<typename T>
	using Ref = std::shared_ptr<T>;
}

#endif // REF_H
