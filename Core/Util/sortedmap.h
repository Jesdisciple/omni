#ifndef SORTEDMAP_H
#define SORTEDMAP_H

#include <string>
#include <map>
#include <list>
#include <forward_list>
#include <functional>

namespace Omni
{
	template<typename K, typename V>
	class SortedMap
	{
	private:
		using list = std::forward_list < V >;
		using iterator = typename list::iterator;
	public:
		using const_iterator = typename list::const_iterator;
		using comparator = std::function < bool(const V & a, const V & b) >;
	private:
		using map = std::map < K, const_iterator > ;
		using map_pair = typename map::value_type;

		map keys;
		list items;

		comparator compare;

	public:
		SortedMap(comparator compare)
			: compare(compare)
		{

		}

		virtual ~SortedMap()
		{

		}

		bool tryAdd(const K key, const V & value)
		{
			auto it = keys.find(key);

			if (it == keys.end())
			{
				items.push_front(value);
				keys.insert(std::make_pair(key, items.begin()));
				items.sort(compare);
				return true;
			}
			else
			{
				return false;
			}
		}

		bool tryGet(const K key, V & value)
		{
			auto it = keys.find(key);
			if (it == keys.end())
			{
				return false;
			}

			value = *it->second;
			return true;
		}

		const_iterator cbegin()
		{
			return items.cbegin;
		}

		const_iterator cend()
		{
			return items.cend();
		}

		iterator begin()
		{
			return items.begin();
		}

		iterator end()
		{
			return items.end();
		}
	};
}

#endif // SORTEDMAP_H