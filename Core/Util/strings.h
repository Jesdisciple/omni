#pragma once

#include <string>
#include <algorithm>
#include <cctype>

namespace Omni
{
	namespace Util
	{
		inline std::string trim(const std::string & s)
		{
			auto wsfront = std::find_if_not(s.begin(), s.end(), [](int c) { return std::isspace(c); });
			auto wsback = std::find_if_not(s.rbegin(), s.rend(), [](int c) { return std::isspace(c); }).base();
			return (wsback <= wsfront ? std::string() : std::string(wsfront, wsback));
		}

		inline std::string trim_front(const std::string & s)
		{
			auto wsfront = std::find_if_not(s.begin(), s.end(), std::isspace);
			return std::string(wsfront, s.end());
		}

		inline std::string trim_back(const std::string & s)
		{
			auto wsback = std::find_if_not(s.rbegin(), s.rend(), std::isspace).base();
			return std::string(s.begin(), wsback);
		}

		template<typename T, typename U>
		inline void join(std::ostream & ss, const T & strings, const U & delimiter)
		{
			if (0 == strings.size()) { return; }

			for (auto it = strings.begin();;)
			{
				ss << *it;
				it++;
				if (it == strings.end()) break;
				ss << delimiter;
			}
		}

		template<typename T, typename U>
		inline std::string join(const T & strings, const U & delimiter)
		{
			std::stringstream ss;
			join(ss, strings, delimiter);
			return ss.str();
		}
	}
}