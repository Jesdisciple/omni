#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <cstdio>

#ifdef _DEBUG
#define PRINT(s) do{ (std::cerr << s << std::endl).flush(); }while(0)
#else
#define PRINT(s) do{ }while(0)
#endif

// http://stackoverflow.com/questions/14409466/simple-hash-functions
inline size_t hash(std::string s)
{
	size_t hashAddress = 0;
	for (size_t i = 0; i < s.length(); i++)
	{
		hashAddress = s.at(i) + (hashAddress << 6) + (hashAddress << 16) - hashAddress;
	}
	return hashAddress;
}

namespace
{
	std::vector<std::string> stack;
}

inline void debug_push(std::string item)
{
	stack.push_back(item);
	for (auto it = stack.begin(); it < stack.end(); it++)
	{
		std::cerr << *it << " ";
	}
	std::cerr << std::endl;
}

inline void debug_pop()
{
	stack.pop_back();
}

template<typename T>
inline void printAsAddress(std::string name, T & t)
{
	printf("%s: %p\n", name.c_str(), t);
}