#pragma once

#include <vector>
#include <map>

#include "../Common/namespace.h"
#include "../Types/interface.h"
#include "../Types/list.h"

namespace Omni
{
	class ParameterList
	{
	private:
		using Parameter = std::pair<Ref<Type>, std::string>;

		Ref<Runtime> runtime;

		std::vector<Parameter> params;

		Namespace ns;

	public:
		ParameterList(Ref<Runtime> runtime, std::vector<Ref<Type>> & types, std::vector<std::pair<Ref<Type>, std::string>> & params);
		ParameterList(Ref<Runtime> runtime, std::vector<Ref<Type>> & types, std::vector<std::string> & names);

		void setDefaults(std::vector<Ref<Value>> defaults);

		Namespace populate(std::vector<Ref<Value>> arguments) const;

		Namespace populate(Ref<List> arguments) const;
	};
}