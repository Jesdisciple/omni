#include "functiontype.h"

#include <sstream>

#include "../Util/debug.h"
#include "function.h"
#include "../Util/strings.h"

using namespace Omni;
using namespace Omni::Util;

FunctionType::FunctionType(Ref<Type> type, Ref<Type> returnType, std::string name, std::initializer_list<Ref<Type>> parameterTypes)
	: Type(typeOf<Function>(), type, name), returnType(returnType)
{
	this->parameterTypes = std::vector<Ref<Type>>(parameterTypes);
}

Ref<Value> FunctionType::create()
{
	auto result = std::make_shared<Function>(shared());
	return result;
}

Ref<Function> FunctionType::create(std::string name, std::vector<std::string> parameterNames, std::function<Ref<Value>()> body)
{
	Ref<FunctionType> type = shared();

	Ref<Function> result = std::make_shared<Function>(type, name, parameterNames, body);

	runtime->declareVariable(type, name, result);

	return result;
}

bool FunctionType::satisfies(Ref<const Type> other) const
{
	Ref<const Value> self = shared_from_this();
	return self == other;
}

bool FunctionType::implements(Ref<const Interface> other) const
{
	Ref<const Value> self = shared_from_this();
	return self == other;
}

Ref<FunctionType> FunctionType::shared()
{
	return std::static_pointer_cast<FunctionType>(shared_from_this());
}

Ref<Type> FunctionType::getReturnType() const
{
	return returnType;
}

const std::vector<Ref<Type>> FunctionType::getParameterTypes() const
{
	return parameterTypes;
}