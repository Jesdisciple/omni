#ifndef FUNCTIONTYPE_H
#define FUNCTIONTYPE_H

#include <memory>
#include <vector>
#include <functional>

#include "../Types/type.h"
#include "../Types/list.h"
#include "../Types/interface.h"
#include "../Common/namespace.h"
#include "parameterlist.h"

namespace Omni
{
	class Function;

	class FunctionType : public Type
	{
	private:
		Ref<Type> returnType;

		std::vector<Ref<Type>> parameterTypes;

	public:
		FunctionType(Ref<Type> type, Ref<Type> returnType, std::string name, std::initializer_list<Ref<Type>> parameterTypes);

		Ref<FunctionType> shared();
		Ref<Type> getReturnType() const;
		const std::vector<Ref<Type>> getParameterTypes() const;

		virtual Ref<Value> create() override;
		Ref<Function> create(std::string name, std::vector<std::string> parameterNames, std::function<Ref<Value>()> body);

		virtual bool satisfies(Ref<const Type> other) const override;
		virtual bool implements(Ref<const Interface> other) const override;
	};
}

#endif // FUNCTIONTYPE_H
