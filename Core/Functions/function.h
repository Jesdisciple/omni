#pragma once

#include "../runtime.h"
#include "../Common/ref.h"

#include "functiontype.h"

namespace Omni
{
	class Function : public Value
	{
	private:
		std::string name;
		
		std::vector<std::string> parameterNames;

		std::function<Ref<Value>(Ref<Runtime>)> body;

	public:
		Function(Ref<FunctionType> type);
		Function(Ref<FunctionType> type, std::string name, std::vector<std::string> parameterNames, std::function<Ref<Value>(Ref<Runtime>)> body);
		Function(Ref<FunctionType> type, std::vector<std::string> parameterNames, std::function<Ref<Value>(Ref<Runtime>)> body);

		void init(std::string name, std::vector<std::string> parameterNames, std::function<Ref<Value>(Ref<Runtime>)> body);
		void init(std::vector<std::string> parameterNames, std::function<Ref<Value>(Ref<Runtime>)> body);

		Ref<Function> shared();

		std::function<Ref<Value>(Ref<List>)> asNative();

		Ref<Value> call(Ref<List> arguments);

		// Inherited via Value
		virtual Ref<Value> copy() override;
		virtual void writeTo(std::ostream & stream) const override;
	};
}