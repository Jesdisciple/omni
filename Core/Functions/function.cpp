#include "function.h"
#include <sstream>
#include "../Util/debug.h"
#include "../Util/strings.h"

using namespace Omni;
using namespace Omni::Util;

Function::Function(Ref<FunctionType> type, std::string name, std::vector<std::string> parameterNames, std::function<Ref<Value>(Ref<Runtime>)> body)
	: Value(type), name(name), parameterNames(parameterNames), body(body)
{
}

Function::Function(Ref<FunctionType> type, std::vector<std::string> parameterNames, std::function<Ref<Value>(Ref<Runtime>)> body)
	: Function(type, "[anonymous function]", parameterNames, body)
{
}

Function::Function(Ref<FunctionType> type)
	: Function(type, "[anonymous function]", {}, nullptr)
{
}

void Function::init(std::vector<std::string> parameterNames, std::function<Ref<Value>(Ref<Runtime>)> body)
{
	this->parameterNames = parameterNames;
	this->body = body;
}

void Function::init(std::string name, std::vector<std::string> parameterNames, std::function<Ref<Value>(Ref<Runtime>)> body)
{
	init(parameterNames, body);
	this->name= name;
}

Ref<Function> Function::shared()
{
	return std::static_pointer_cast<Function>(shared_from_this());
}

std::function<Ref<Value>(Ref<List>)> Function::asNative()
{
	return[ptr = shared()](Ref<List> args)
	{
		return ptr->call(args);
	};
}

Ref<Value> Function::call(Ref<List> arguments)
{
	auto self = this;
	auto type = std::static_pointer_cast<FunctionType>(getType());
	auto paramTypes = type->getParameterTypes();
	ParameterList paramList{ runtime, paramTypes, parameterNames };

	Ref<Value> result = nullptr;

	runtime->scope(paramList.populate(arguments), body);

	return result;
}

Ref<Value> Function::copy()
{
	auto type = std::static_pointer_cast<FunctionType>(getType());
	return std::make_shared<Function>(type, name, parameterNames, body);
}

void Function::writeTo(std::ostream & stream) const
{
	auto type = std::static_pointer_cast<FunctionType>(getType());
	stream << "[ " << type->getReturnType() << name << " ( ";
	join(stream, type->getParameterTypes(), ", ");
	stream << " ) ]";
}