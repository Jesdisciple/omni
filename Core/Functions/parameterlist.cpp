#include "parameterlist.h"
#include "../runtime.h"

using namespace Omni;

ParameterList::ParameterList(Ref<Runtime> runtime, std::vector<Ref<Type>> & types, std::vector<std::pair<Ref<Type>, std::string>> & params)
	: runtime(runtime)
{
	bool error = false;

	for (size_t i = 0; i < params.size() && !error; i++)
	{
		auto param = params[i];
		if (types[i] != param.first)
		{
			error = true;
			// TODO: exception
		}
		else
		{
			ns.declareVariable(param.first, param.second);
		}
	}

	if (!error)
	{
		this->params = params;
		return;
	}
}

ParameterList::ParameterList(Ref<Runtime> runtime, std::vector<Ref<Type>> & types, std::vector<std::string> & names)
	: runtime(runtime)
{
	if (types.size() != names.size())
	{
		// TODO: exception
		return;
	}

	for (size_t i = 0; i < names.size(); i++)
	{
		params.push_back(std::make_pair(types[i], names[i]));
		ns.declareVariable(types[i], names[i]);
	}
}

void ParameterList::setDefaults(std::vector<Ref<Value>> defaults)
{
	Parameter param;
	Ref<Value> arg;
	auto paramCount = params.size();
	auto argCount = defaults.size();

	// [1, size()] because i is subtracted from the length
	for (size_t i = 1; i <= params.size(); i++)
	{
		param = params[paramCount - i];
		arg = defaults[argCount - i];

		ns.assignVariable(param.second, arg);
	}
}

Namespace ParameterList::populate(std::vector<Ref<Value>> arguments) const
{
	return populate(runtime->list(arguments));
}

Namespace ParameterList::populate(Ref<List> arguments) const
{
	auto result = ns;

	if (params.size() != arguments->size())
	{
		// TODO: exception
		return result;
	}

	size_t size = params.size();

	Parameter param;

	for (size_t i = 0; i < size; i++)
	{
		result.assignVariable(params[i].second, arguments->at(i));
	}

	return result;
}
