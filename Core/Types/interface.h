#ifndef INTERFACE_H
#define INTERFACE_H

#include <memory>
#include <unordered_map>

#include "type.h"

namespace Omni
{
	class Interface : public Type
	{
		private:
			using Map = std::unordered_map<std::string, Ref<Type>>;
			Map map;

			Ref<Type> objectType;

		public:
			Interface(Ref<Type> type);

			void init(Ref<Type> objectType, std::initializer_list<std::pair<Ref<Type>, std::string>> pairs);

			using const_iterator = Map::const_iterator;

			void insert(const std::pair<std::string, Ref<Type>> pair);

			const_iterator cbegin() const;
			const_iterator cend() const;

			size_t size() const;

			bool getTypeOfVariable(std::string name, Ref<Type> & result);

			virtual Ref<Value> create();

			virtual bool satisfies(Ref<const Type> other) const override;

			virtual bool implements(Ref<const Interface> other) const override;

			virtual bool equals(Ref<const Value> other) const override;

			friend class Runtime;
	};
}

#endif // INTERFACE_H
