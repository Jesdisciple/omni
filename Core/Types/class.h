#ifndef Class_H
#define Class_H

#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <utility>

#include "value.h"
#include "type.h"
#include "simpleobject.h"
#include "interface.h"

namespace Omni
{
	class Object;

	class Class : public Type
	{
		private:
			Ref<Interface> interface;
			Ref<Class> parent;
			Ref<Type> objectType;

		public:
			Class(Ref<Type> type);

			void init(Ref<Type> objectType, Ref<Interface> interface);
			void init(Ref<Type> objectType, Ref<Class> parent, Ref<Interface> interface);

            //static Ref<Class> create(Ref<Type> type);
			//static Ref<Class> create(Ref<Type> type, std::string name, Ref<Type> objectType, Ref<Interface> interface);
			//static Ref<Class> create(Ref<Type> type, std::string name, Ref<Type> objectType, Ref<Class> parent, Ref<Interface> interface);

			void init(std::string name, Ref<Type> objectType, Ref<Interface> interface);
			void init(std::string name, Ref<Type> objectType, Ref<Class> parent, Ref<Interface> interface);

			Ref<Type> getObjectType() const;
			Ref<Interface> getInterface() const;

			virtual Ref<Value> create();

			virtual bool satisfies(Ref<const Type> other) const override;

			virtual bool implements(Ref<const Interface> i) const override;

			virtual void writeTo(std::ostream & stream) const override;

			bool extends(Ref<const Class> c) const;

			friend class Runtime;
	};
}

#endif // Class_H
