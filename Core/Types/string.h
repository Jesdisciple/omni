#pragma once

#include <string>

#include "value.h"

namespace Omni
{
	class String : public Value
	{
	private:
		std::string value;

	public:
		String(Ref<Type> type);

		void init(std::string value);

		std::string getValue();

		virtual Ref<Value> copy() override;
		virtual void writeTo(std::ostream & stream) const override;
	};
}