#ifndef TYPE_H
#define TYPE_H

#include <string>
#include <memory>

#include "value.h"

namespace Omni
{
	class Void;
	class Value;
	class PointerType;
	class PrimitiveType;
	class Interface;

	class Type : public Value
	{
		private:
			const NativeType nativeType;

			std::string name;

			Ref<PointerType> pointerType;

			Type(Runtime * runtime);

			void init(Ref<Type> type, std::string name);

			Ref<Type> shared();

		protected:
			Type(NativeType nativeType, Ref<Type> type);
			Type(NativeType nativeType, Ref<Type> type, std::string name);

			void init(std::string name);

		public:
			~Type();

			NativeType toNative() const;

			Ref<PointerType> getPointerType(Ref<Type> type);

			virtual bool isPointerType() const;

			virtual Ref<Value> copy() override;

			virtual Ref<Value> create() = 0;

			virtual bool satisfies(Ref<const Type> other) const = 0;

			virtual bool implements(Ref<const Interface> other) const = 0;

			virtual std::string getName() const;

			virtual void writeTo(std::ostream & stream) const override;

			friend class PrimitiveType;
	};
}

#endif // TYPE_H
