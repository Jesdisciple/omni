#include "object.h"
#include "class.h"
#include "../Common/namespace.h"
#include "../runtime.h"

using namespace Omni;

Object::Object(Ref<Class> c)
    : SimpleObject(c->getObjectType(), c->getInterface()), class_(c)
{
}

Ref<Value> Object::copy()
{
    auto result = std::make_shared<Object>(class_);
	result->ns = Namespace(ns);
	return result;
}

Ref<Type> Object::getType() const
{
	return class_;
}

void Object::writeTo(std::ostream & stream) const
{
	stream << "<" << class_->getName() << " object>";
}
