#include "string.h"

using namespace Omni;

String::String(Ref<Type> type)
	: Value(type)
{
}

void String::init(std::string value)
{
	this->value = value;
}

std::string String::getValue()
{
	return value;
}

Ref<Value> String::copy()
{
	auto result = std::make_shared<String>(getType());
	result->init(value);
	return result;
}

void String::writeTo(std::ostream & stream) const
{
	stream << value;
}
