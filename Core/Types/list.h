#pragma once

#include <vector>

#include "value.h"
#include "../Util/strings.h"

namespace Omni
{
	class List : public Value
	{
	private:
		friend class List;

		using vector = std::vector<Ref<Value>>;
		vector items;

	public:
		class iterator :
			public std::iterator<std::random_access_iterator_tag, Ref<Value>>
		{
		private:
			friend class List;

			vector::iterator base;

			iterator(vector::iterator base);

		public:
			Ref<Value>& operator*();
			Ref<Value> operator->();
			iterator& operator++();
			iterator operator++(int);
			iterator& operator--();
			iterator operator--(int);
			bool operator==(iterator other);
			bool operator!=(iterator other);
		};

		size_t size() const;

		Ref<Value> at(size_t index);
		Ref<Value> front();
		Ref<Value> back();

		iterator begin();
		iterator end();

		List(Ref<Type> type);
		List(Ref<Type> type, vector items);
		List(const List & other);

		void add(Ref<Value> item);

		template<typename... T>
		void emplace_back(T... args)
		{
			push_back(std::make_shared<Value>(args...));
		}

		virtual Ref<Value> copy() override;
		virtual void writeTo(std::ostream & stream) const override;
	};
}