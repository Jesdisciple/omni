#include "list.h"

using namespace Omni;
using namespace Omni::Util;

List::List(Ref<Type> type, vector items)
	: Value(type), items(items)
{
}

List::List(Ref<Type> type)
	: Value(type)
{
}

List::List(const List & other)
	: Value(other.getType()), items(other.items)
{
}

void List::add(Ref<Value> item)
{
	items.push_back(item);
}

Ref<Value> List::copy()
{
	return Ref<List>(new List(getType(), items));
}

void List::writeTo(std::ostream & stream) const
{
	stream << "(";
	join(stream, items, ", ");
	stream << ")";
}

size_t List::size() const
{
	return items.size();
}

Ref<Value> List::at(size_t index)
{
	return items[index];
}

Ref<Value> List::front()
{
	return items.front();
}

Ref<Value> List::back()
{
	return items.back();
}

List::iterator List::begin()
{
	return iterator(items.begin());
}

List::iterator List::end()
{
	return iterator(items.end());
}

Ref<Value>& List::iterator::operator*()
{
	return *base;
}
Ref<Value> List::iterator::operator->()
{
	return *base;
}
List::iterator::iterator(vector::iterator base)
	: base(base)
{
}
List::iterator& List::iterator::operator++()
{
	++base; return *this;
}
List::iterator List::iterator::operator++(int)
{
	auto result = *this; ++base; return result;
}
List::iterator& List::iterator::operator--()
{
	--base; return *this;
}
List::iterator List::iterator::operator--(int)
{
	auto result = *this; --base; return result;
}
bool List::iterator::operator==(iterator other)
{
	return base == other.base;
}
bool List::iterator::operator!=(iterator other)
{
	return !operator==(other);
}