#include "pointer.h"
#include "pointertype.h"
#include "../runtime.h"

using namespace Omni;

Pointer::Pointer(Ref<PointerType> type, Ref<Value> value)
    : Value(type), value(value)
{
}

Ref<Value> Pointer::getValue()
{
	return value;
}

Ref<Value> Pointer::copy()
{
	auto pointerType = std::static_pointer_cast<PointerType>(type);
    return std::make_shared<Pointer>(pointerType, value);
}

void Pointer::writeTo(std::ostream &stream) const
{
	stream << "<" << getType()->getName() << " " << value << ">";
}
