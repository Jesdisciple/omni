#ifndef SIMPLEOBJECT_H
#define SIMPLEOBJECT_H

#include <unordered_map>

#include "../Common/namespace.h"

#include "value.h"
#include "interface.h"

namespace Omni
{
	class SimpleObject : public Value
	{
		protected:
			Namespace ns;

        public:
			SimpleObject(Ref<Type> type);
			SimpleObject(Ref<Type> type, Ref<Interface> i);
			SimpleObject(Ref<Type> type, Namespace ns);

			void init(Ref<Interface> i);
			void init(Namespace ns);

			virtual Ref<Value> copy();

			Ref<Value> resolveVariable(std::string name);
			void assignVariable(std::string name, Ref<Value> value);
			Ref<Type> getTypeOfVariable(std::string name);

			virtual void writeTo(std::ostream & stream) const;

			Namespace * getNamespace();

			friend class Runtime;
	};
}

#endif // SIMPLEOBJECT_H
