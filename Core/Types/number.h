#ifndef VALUE_H
#define VALUE_H

#include <memory>

#include "value.h"
#include "type.h"
#include "object.h"

namespace Omni
{
	class Number : public Value
	{
	private:
		double value;

	public:
		Number(Ref<Type> type, double value = 0);

		virtual Ref<Value> copy();

		double getValue() const;

		void setValue(double value);

		virtual void writeTo(std::ostream & stream) const;

		virtual bool implements(Ref<Interface> other);

		friend class Runtime;
	};
}

#endif // VALUE_H
