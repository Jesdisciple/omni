#include <iostream>

#include "simpleobject.h"
#include "../runtime.h"

using namespace Omni;

SimpleObject::SimpleObject(Ref<Type> type)
	: Value(type), ns()
{
}

SimpleObject::SimpleObject(Ref<Type> type, Ref<Interface> i)
	: SimpleObject(type)
{
	this->init(i);
}

SimpleObject::SimpleObject(Ref<Type> type, Namespace ns)
	: SimpleObject(type)
{
	this->init(ns);
}

void SimpleObject::init(Ref<Interface> i)
{
	for(auto it = i->cbegin(); it != i->cend(); it++)
	{
		ns.declareVariable(it->second, it->first);
	}
}

void SimpleObject::init(Namespace ns)
{
	this->ns = Namespace(ns);
}

Ref<Value> SimpleObject::copy()
{
	return std::make_shared<SimpleObject>(type, ns);
}

Ref<Value> SimpleObject::resolveVariable(std::string name)
{
	return ns.resolveVariable(name);
}

void SimpleObject::assignVariable(std::string name, Ref<Value> value)
{
	//std::cout << "value type: " << *value->getType() << "\n";
	ns.assignVariable(name, value);
}

Ref<Type> SimpleObject::getTypeOfVariable(std::string name)
{
	return ns.getTypeOfVariable(name);
}

void SimpleObject::writeTo(std::ostream & stream) const
{
	stream << "<object>";
}

Namespace * Omni::SimpleObject::getNamespace()
{
	return &ns;
}