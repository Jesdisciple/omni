#include "value.h"
#include "type.h"
#include "pointer.h"

using namespace Omni;

Value::Value(Runtime * runtime)
	: runtime(runtime), type(nullptr)
{
}

Value::Value(Ref<Type> type)
    : runtime(type->getRuntime()), type(type)
{
}

Value::~Value()
{
	type = nullptr;
}

Runtime * Value::getRuntime() const
{
	return runtime;
}

Ref<Type> Value::getType() const
{
	return type;
}

Ref<Pointer> Value::getPointer(Ref<Type> pointerTypeType)
{
    return std::make_shared<Pointer>(type->getPointerType(pointerTypeType), this->shared_from_this());
}

bool Value::equals(Ref<const Value> other) const
{
	return shared_from_this() == other;
}

NativeType Value::getNativeType() const
{
	return getType()->toNative();
}