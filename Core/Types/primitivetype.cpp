#include "primitivetype.h"
#include "value.h"
#include "../runtime.h"

using namespace Omni;

PrimitiveType::PrimitiveType(Runtime * runtime)
	: Type(runtime), cstor(nullptr)
{

}

PrimitiveType::PrimitiveType(NativeType nativeType, Ref<Type> type)
    : Type(nativeType, type), cstor(nullptr)
{
}

PrimitiveType::PrimitiveType(NativeType nativeType, Ref<Type> type, std::string name)
    : Type(nativeType, type), cstor(nullptr)
{
	Type::init(name);
}

void PrimitiveType::init(std::function<Ref<Value>()> cstor)
{
	this->cstor = cstor;
}

void PrimitiveType::init(Ref<Type> type, std::function<Ref<Value>()> cstor)
{
	this->type = type;
	this->cstor = cstor;
}

Ref<PrimitiveType> PrimitiveType::root(Runtime * runtime)
{
	auto result = Ref<PrimitiveType>(new PrimitiveType(runtime));
	result->init(result, [result]()
	{
		return std::make_shared<PrimitiveType>(result->getNativeType(), result);
	});
	return std::static_pointer_cast<PrimitiveType>(result);
}

Ref<Value> PrimitiveType::create()
{
    auto result = cstor();
	return result;
}

bool PrimitiveType::satisfies(Ref<const Type> other) const
{
	return this->equals(other);
}

bool PrimitiveType::implements(Ref<const Interface> other) const
{
	return false;
}

void PrimitiveType::writeTo(std::ostream & stream) const
{
    stream << "<type " << getName() << ">";
}
