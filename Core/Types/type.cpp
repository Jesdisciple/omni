#include "type.h"
#include "pointertype.h"
#include "primitivetype.h"

using namespace Omni;

Type::Type(Runtime * runtime)
	: Value(runtime), name("type"), nativeType(typeOf<Type>())
{
}

Type::Type(NativeType nativeType, Ref<Type> type)
	: Value(type), name("<anonymous type>"), nativeType(nativeType)
{
}

Type::Type(NativeType nativeType, Ref<Type> type, std::string name)
	: Value(type), name(name), nativeType(nativeType)
{
}

NativeType Type::toNative() const
{
	return nativeType;
}

void Type::init(Ref<Type> type, std::string name)
{
	this->type = type;
	this->name = name;
}

Ref<Type> Type::shared()
{
	return std::static_pointer_cast<Type>(shared_from_this());
}

void Type::init(std::string name)
{
	this->name = name;
}

Type::~Type()
{

}

Ref<Value> Type::copy()
{
	return shared_from_this();
}

Ref<PointerType> Type::getPointerType(Ref<Type> type)
{
	if(pointerType.use_count() == 0)
	{
		pointerType = Ref<PointerType>(new PointerType(type));
	}
	return pointerType;
}

bool Type::isPointerType() const
{
	return false;
}

std::string Type::getName() const
{
	return name;
}

void Type::writeTo(std::ostream & stream) const
{
	stream << "<type " << getName() << ">";
}