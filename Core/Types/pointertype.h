#ifndef POINTERTYPE_H
#define POINTERTYPE_H

#include <memory>

#include "type.h"

namespace Omni
{
	class PointerType : public Type
	{
	private:
		Ref<Type> valueType;

	public:
		PointerType(Ref<Type> type);

		virtual bool isPointerType() const;

		void init(Ref<Type> valueType);

		Ref<Type> getValueType() const;

		virtual Ref<Value> create();

		Ref<Value> create(Ref<Type> type);

		virtual bool satisfies(Ref<const Type> other) const override;

		virtual bool implements(Ref<const Interface> other) const override;
	};
}

#endif // POINTERTYPE_H
