#ifndef USERTYPE_H
#define USERTYPE_H

#include <memory>
#include <string>
#include <functional>

#include "../Common/ref.h"

#include "type.h"

namespace Omni
{
	class Value;
	class Runtime;

	class PrimitiveType : public Type
	{
	private:
		std::function<std::shared_ptr<Value>()> cstor;

	public:
		PrimitiveType(Runtime * runtime);
		PrimitiveType(NativeType nativeType, std::shared_ptr<Type> type);
		PrimitiveType(NativeType nativeType, std::shared_ptr<Type> type, std::string name);

		void init(std::function<Ref<Value>()> cstor);
		void init(std::shared_ptr<Type> type, std::function<Ref<Value>()> cstor);

		static std::shared_ptr<PrimitiveType> root(Runtime * runtime);

		template<typename T>
		static std::shared_ptr<PrimitiveType> createType(std::shared_ptr<Type> type, std::string name)
		{
			std::shared_ptr<PrimitiveType> result = std::make_shared<PrimitiveType>(typeOf<T>(), type, name);
			result->init([result]()
			{
				return std::make_shared<T>(result);
			});
			return result;
		}

		virtual std::shared_ptr<Value> create();

		virtual bool satisfies(std::shared_ptr<const Type> other) const override;

		virtual bool implements(std::shared_ptr<const Interface> other) const override;

		virtual void writeTo(std::ostream & stream) const;
	};
}

#endif // USERTYPE_H
