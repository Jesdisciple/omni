#include "interface.h"
#include "simpleobject.h"
#include "../runtime.h"

using namespace Omni;

Interface::Interface(Ref<Type> type)
    : Type(typeOf<Interface>(), type)
{
}

void Interface::init(Ref<Type> objectType, std::initializer_list<std::pair<Ref<Type>, std::string>> pairs)
{
	this->objectType = objectType;

	for(std::pair<Ref<Type>, std::string> pair : pairs)
	{
		insert(std::make_pair(pair.second, pair.first));
	}
}

void Interface::insert(const std::pair<std::string, Ref<Type>> pair)
{
	map.insert(pair);
}

Interface::const_iterator Interface::cbegin() const
{
	return map.cbegin();
}

Interface::const_iterator Interface::cend() const
{
	return map.cend();
}

size_t Interface::size() const
{
	return map.size();
}

bool Interface::getTypeOfVariable(std::string name, Ref<Type> & result)
{
	auto it = map.find(name);
	if(it != map.end())
	{
		result = it->second->getType();
		return true;
	}
	return false;
}

Ref<Value> Interface::create()
{
	return std::make_shared<SimpleObject>(objectType, std::static_pointer_cast<Interface>(this->shared_from_this()));
}

bool Interface::satisfies(Ref<const Type> other) const
{
	if(other->getType() == getType())
	{
		auto face = std::static_pointer_cast<const Interface>(other);
		return implements(face);
	}
	return false;
}

bool Interface::implements(Ref<const Interface> other) const
{
	if(this->equals(other))
	{
		return true;
	}

	for(auto it = other->cbegin(); it != other->cend(); it++)
	{
		auto found = map.find(it->first);
		if(found == map.end())
		{
			return false;
		}
		else
		{
			if(!found->second->satisfies(it->second))
			{
				return false;
			}
		}
	}

	return true;
}

bool Interface::equals(Ref<const Value> other) const
{
	return shared_from_this() == other;

	/*
	if(other->type != type)
	{
		return false;
	}

	Ref<Interface> face = std::static_pointer_cast<Interface>(other);
	return
	*/
}
