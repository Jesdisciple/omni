#ifndef OBJECT_H
#define OBJECT_H

#include "value.h"
#include "type.h"
#include "class.h"
#include "simpleobject.h"

namespace Omni
{
	class Object : public SimpleObject
	{
		private:
			Ref<Class> class_;

		public:
			Object(Ref<Class> c);

			virtual Ref<Value> copy();

			virtual Ref<Type> getType() const;

			virtual void writeTo(std::ostream & stream) const;

			friend class Runtime;
	};
}

#endif // OBJECT_H
