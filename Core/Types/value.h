#ifndef PRIMITIVE_H
#define PRIMITIVE_H

#include <memory>
#include <cstring>
#include <ostream>

#include "../Common/ref.h"
#include "../Common/nativetype.h"

namespace Omni
{
	class Type;
	class Pointer;
	class Runtime;

	class Value : public std::enable_shared_from_this<Value>
	{
	private:
		Value(Runtime * runtime);

	protected:
		Runtime * runtime;
		Ref<Type> type;

	public:
		Value(Ref<Type> type);
		~Value();
		Runtime * getRuntime() const;

		NativeType getNativeType() const;

		Ref<Pointer> getPointer(Ref<Type> pointerTypeType);

		template<typename T>
		Ref<T> as()
		{
			return std::dynamic_pointer_cast<T>(shared_from_this());
		}

		virtual Ref<Type> getType() const;
		virtual bool equals(Ref<const Value>) const;

		virtual Ref<Value> copy() = 0;
		virtual void writeTo(std::ostream & stream) const = 0;

		friend class Type;
	};

	inline std::ostream & operator <<(std::ostream & stream, const Value & value)
	{
		value.writeTo(stream);
		return stream;
	}

	inline std::ostream & operator <<(std::ostream & stream, const Ref<Value> & value)
	{
		value->writeTo(stream);
		return stream;
	}

	inline std::ostream & operator <<(std::ostream & stream, const char * str)
	{
		stream.write(str, strlen(str));
		return stream;
	}
}

#endif // PRIMITIVE_H
