#include "class.h"
#include "object.h"
#include "type.h"
#include "../runtime.h"

using namespace Omni;

Class::Class(Ref<Type> type)
	: Type(typeOf<Class>(), type, "<anonymous class>"), parent(nullptr)
{
}

/*
Ref<Class> Class::create(Ref<Type> type, std::string name, Ref<Type> objectType, Ref<Interface> interface)
{
	Ref<Class> result = create(type);
	result->init(name, objectType, interface);
	return result;
}

Ref<Class> Class::create(Ref<Type> type, std::string name, Ref<Type> objectType, Ref<Class> parent, Ref<Interface> interface)
{
	Ref<Class> result = create(type);
	result->init(name, objectType, parent, interface);
	return result;
}
*/

void Class::init(Ref<Type> objectType, Ref<Class> parent, Ref<Interface> interface)
{
	init(objectType, interface);
	this->parent = parent;

	// inheritance
	auto super = parent->interface;
	Ref<Type> type;
	for(auto it = super->cbegin(); it != super->cend(); it++)
	{
		//
		if(!interface->getTypeOfVariable(it->first, type))
		{
			interface->insert(*it);
		}
	}
}

void Class::init(Ref<Type> objectType, Ref<Interface> interface)
{
	this->objectType = objectType;
	this->interface = interface;
}

void Class::init(std::string name, Ref<Type> objectType, Ref<Interface> interface)
{
	Type::init(name);
	init(objectType, interface);
}


void Class::init(std::string name, Ref<Type> objectType, Ref<Class> parent, Ref<Interface> interface)
{
	Type::init(name);
	init(objectType, parent, interface);
}

Ref<Value> Class::create()
{
    return std::make_shared<Object>(std::static_pointer_cast<Class>(this->shared_from_this()));
}

Ref<Type> Class::getObjectType() const
{
	return objectType;
}

Ref<Interface> Class::getInterface() const
{
	return interface;
}


bool Class::satisfies(Ref<const Type> other) const
{
	if(other == runtime->getObjectType())
	{
		return true;
	}

	if(other->getType() == runtime->getClassType())
	{
		return extends(std::static_pointer_cast<const Class>(other));
	}

	if(other->getType() == runtime->getInterfaceType())
	{
		return implements(std::static_pointer_cast<const Interface>(other));
	}

	return false;
}

void Class::writeTo(std::ostream & stream) const
{
	stream << "<class " << getName() << ">";
}

bool Class::implements(Ref<const Interface> i) const
{
	return interface->implements(i);
}

bool Class::extends(Ref<const Class> c) const
{
	return false;
}
