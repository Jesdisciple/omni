#include <memory>

#include "number.h"
#include "../runtime.h"

using namespace Omni;

Number::Number(Ref<Type> type, double value)
    : Value(type), value(value)
{
}

Ref<Value> Number::copy()
{
    return std::make_shared<Number>(type, value);
}

double Number::getValue() const
{
	return value;
}

void Number::setValue(double f)
{
	value = f;
}

bool Number::implements(Ref<Interface> other)
{
	return false;
}

void Number::writeTo(std::ostream & stream) const
{
	stream << "<number " << value << ">";
}
