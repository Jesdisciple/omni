#pragma once

#include <string>

#include "../Common/nativetype.h"
#include "../Common/ref.h"
#include "type.h"

namespace Omni
{
	class Void : public Value
	{
	public:
		Void(Ref<Type> parent);

		virtual Ref<Value> copy() override;
		virtual void writeTo(std::ostream & stream) const override;

		virtual bool equals(Ref<const Value> other) const override
		{
			return typeOf<Void>() == typeOf(other);
		}
	};
}