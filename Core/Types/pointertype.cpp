#include "pointertype.h"
#include "pointer.h"
#include "../runtime.h"

using namespace Omni;

PointerType::PointerType(Ref<Type> type)
    : Type(typeOf<PointerType>(), type, "(ptr " + type->getName() + ")"), valueType(nullptr)
{
}

bool PointerType::isPointerType() const
{
	return true;
}

void PointerType::init(Ref<Type> valueType)
{
	this->valueType = valueType;
}

Ref<Type> PointerType::getValueType() const
{
	return valueType;
}

Ref<Value> PointerType::create()
{
	return create(nullptr);
}

Ref<Value> PointerType::create(Ref<Type> type)
{
    return type->getRuntime()->make<PointerType>(type);
}

bool PointerType::satisfies(Ref<const Type> other) const
{
	if(this->equals(other))
	{
		return true;
	}

	if(getType() != other->getType())
	{
		return false;
	}

	auto ptr = std::static_pointer_cast<const PointerType>(other);
	return getValueType()->satisfies(ptr->getValueType());
}

bool PointerType::implements(Ref<const Interface> other) const
{
	return false;
}
