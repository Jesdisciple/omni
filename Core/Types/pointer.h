#ifndef POINTER_H
#define POINTER_H

#include "value.h"

namespace Omni
{
	class PointerType;

	class Pointer : public Value
	{
		private:
			Ref<Value> value;

		public:
			Pointer(Ref<PointerType> type, Ref<Value> value = nullptr);

			Ref<Value> getValue();

			virtual Ref<Value> copy() override;

			virtual void writeTo(std::ostream &stream) const;

			friend class Runtime;
	};
}

#endif // POINTER_H
