#include "primitivetype.h"
#include "void.h"

using namespace Omni;

Void::Void(Ref<Type> type)
	: Value(type)
{
}

Ref<Value> Void::copy()
{
	return shared_from_this();
}

void Void::writeTo(std::ostream & stream) const
{
}