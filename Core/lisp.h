#pragma once

#include <iostream>

#include "Common/ref.h"
#include "Types/list.h"
#include "Lexer/lexeme.h"
#include "Lexer/token.h"
#include "Lexer/lexer.h"
#include "runtime.h"

namespace Omni
{
	class Lisp
	{
	private:
		Ref<Runtime> runtime;

		Lexer lexer;

		Ref<Token> numberToken, lparenToken, rparenToken, identifierToken;

		Ref<Value> plus(Runtime * runtime);
		Ref<Value> minus(Runtime * runtime);

		Ref<List> parse(std::vector<Lexeme> & lex);

	public:
		Lisp();

		Ref<List> eval(std::string source);
	};
}