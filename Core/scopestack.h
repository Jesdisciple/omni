#ifndef SCOPESTACK_H
#define SCOPESTACK_H

#include <vector>
#include <functional>

#include "Common/namespace.h"

namespace Omni
{
	class ScopeStack
	{
		private:
			std::vector<Namespace> stack;
			Namespace global;

		public:
			ScopeStack();
			~ScopeStack();
			const ScopeStack & operator =(const ScopeStack & other);

			bool findVariable(std::string name, Namespace *& ns, Namespace::iterator & it);
			void declareVariable(Ref<Type> type, std::string name);

			void assignVariable(std::string name, Ref<Value> value);

			Ref<Value> resolveVariable(std::string name);
			bool tryResolveVariable(std::string name, Ref<Value> & out_value);

			Ref<Type> getTypeOfVariable(std::string name);

			void scope(Namespace & ns, std::function<void()> fn);
	};
}

#endif // SCOPESTACK_H
