#include <iostream>

#include "Functions/function.h"

#include "Types/string.h"

#include "runtime.h"

using namespace Omni;

Runtime::Runtime()
{
	rootType = PrimitiveType::root(this);
	voidType = PrimitiveType::createType<Void>(rootType, "void");
	numberType = PrimitiveType::createType<Number>(rootType, "number");
	objectType = PrimitiveType::createType<SimpleObject>(rootType, "object");
	classType = PrimitiveType::createType<Class>(rootType, "class");
	interfaceType = PrimitiveType::createType<Interface>(rootType, "interface");

	listType = PrimitiveType::createType<List>(rootType, "list");
	stringType = PrimitiveType::createType<String>(rootType, "string");

	declareVariable(rootType, "type", rootType);
	declareVariable(rootType, "void", voidType);
	declareVariable(rootType, "number", numberType);
	declareVariable(rootType, "object", objectType);
	declareVariable(rootType, "class", classType);
	declareVariable(rootType, "interface", interfaceType);

	declareVariable(rootType, "list", listType);
	declareVariable(rootType, "string", stringType);

	auto transformType = getFunctionType(listType, "Transform", { listType });
	transformType->create("meta", {"code"}, [this]()
	{
		//meta(input);

		return list();
	});
	export("meta");

	auto strongSilentType = getFunctionType(voidType, "NaryProcedure", { listType });
	strongSilentType->create("export", {"id"}, [this]()
	{
		export(resolveVariable("id")->as<String>()->getValue());

		return voidType->create();
	});
}

Runtime::~Runtime()
{
	rootType = nullptr;
	voidType = nullptr;
	numberType = nullptr;
	objectType = nullptr;
	classType = nullptr;
	interfaceType = nullptr;

	listType = nullptr;
	stringType = nullptr;
}

Runtime & Runtime::operator =(Runtime & other)
{
	stack = other.stack;

	rootType = other.rootType;
	voidType = other.voidType;
	numberType = other.numberType;
	objectType = other.objectType;
	classType = other.classType;
	interfaceType = other.interfaceType;

	listType = other.listType;
	stringType = other.stringType;

	return *this;
}

Ref<PrimitiveType> Runtime::getTypeType() const
{
	return rootType;
}

Ref<PrimitiveType> Runtime::getVoidType() const
{
	return voidType;
}

Ref<PrimitiveType> Runtime::getNumberType() const
{
	return numberType;
}

Ref<PrimitiveType> Runtime::getListType() const
{
	return listType;
}

Ref<PrimitiveType> Runtime::getObjectType() const
{
	return objectType;
}

Ref<PrimitiveType> Runtime::getClassType() const
{
	return classType;
}

Ref<PrimitiveType> Runtime::getInterfaceType() const
{
	return interfaceType;
}

Ref<FunctionType> Runtime::getFunctionType(Ref<Type> returnType, std::string name, std::initializer_list<Ref<Type>> paramTypes)
{
	return std::make_shared<FunctionType>(rootType, returnType, name, paramTypes);
}

Ref<Class> Runtime::defineClass(std::string name, Ref<Interface> i)
{
	auto result = std::static_pointer_cast<Class>(classType->create());
	result->init(name, objectType, i);

	declareVariable(classType, name, result);

	return result;
}

void Runtime::declareVariable(Ref<Type> type, std::string name)
{
	stack.declareVariable(type, name);
}

void Runtime::declareVariable(Ref<Type> type, std::string name, Ref<Value> value)
{
	declareVariable(type, name);
	assignVariable(name, value);
}

void Runtime::assignVariable(std::string name, Ref<Value> value)
{
	stack.assignVariable(name, value);
}

Ref<Value> Runtime::resolveVariable(std::string name, bool exportedOnly)
{
	if (!exportedOnly || exportedSymbols.find(name) != exportedSymbols.end())
	{
		return stack.resolveVariable(name);
	}
	// TODO: exception
	return nullptr;
}

Ref<Value> Runtime::resolveVariable(Ref<Identifier> id, bool exportedOnly)
{
	return resolveVariable(id->getName());
}

bool Runtime::tryResolveVariable(std::string name, Ref<Value> & out_value, bool exportedOnly)
{
	if (!exportedOnly || exportedSymbols.find(name) != exportedSymbols.end())
	{
		return stack.tryResolveVariable(name, out_value);
	}
	return false;
}

bool Runtime::tryResolveVariable(Ref<Identifier> id, Ref<Value>& out_value, bool exportedOnly)
{
	return tryResolveVariable(id->getName(), out_value);
}

Ref<Type> Runtime::getTypeOfVariable(std::string name)
{
	return stack.getTypeOfVariable(name);
}

Ref<List> Runtime::preprocess(Ref<List> original)
{
	Ref<Value> value, temp;

	Ref<List> result = list();

	for (auto it = original->begin(); it != original->end(); it++)
	{
		value = *it;

		auto list = value->as<List>();
		if (list != nullptr)
		{
			result->add(preprocess(list));
			continue;
		}

		auto id = value->as<Identifier>();
		if (id != nullptr && getMetaRuntime()->tryResolveVariable(id, temp, true))
		{
			result->add(temp);
			continue;
		}

		result->add(value);
	}

	return execute(result);
}

Ref<List> Runtime::compile(Ref<List> original)
{
	Ref<Value> value, temp;

	Ref<List> result = list();

	original = preprocess(original);

	for (auto it = original->begin(); it != original->end(); it++)
	{
		value = *it;

		auto list = value->as<List>();
		if (list != nullptr)
		{
			result->add(compile(list));
			continue;
		}

		auto id = value->as<Identifier>();
		if (id != nullptr && tryResolveVariable(id, temp))
		{
			result->add(temp);
			continue;
		}

		result->add(value);
	}

	return result;
}

Ref<List> Runtime::execute(Ref<List> original)
{
	Ref<List> result = list();

	for (auto item : *original)
	{
		result->add(resolve(item));
	}

	return result;
}

Ref<Value> Runtime::resolve(Ref<Value> value)
{
	Ref<List> list = value->as<List>();

	if (list == nullptr)
	{
		return value;
	}

	list = list->copy()->as<List>();

	auto it = list->begin();
	if (it == list->end())
	{
		// TODO: Empty list. Not sure what to do with this.
	}

	Ref<Value> first = resolve(*it);

	Ref<Function> f = std::dynamic_pointer_cast<Function>(first);

	if (f == nullptr)
	{
		for (it; it != list->end(); it++)
		{
			*it = resolve(*it);
		}

		return list;
	}
	else
	{
		auto rest = this->list();
		for (it++; it != list->end(); it++)
		{
			rest->add(resolve(*it));
		}

		return f->call(rest);
	}
}

void Runtime::scope(std::function<void(Ref<Runtime>)> fn)
{
	Namespace ns{};
	scope(ns, fn);
}

void Runtime::scope(Namespace & ns, std::function<void(Ref<Runtime>)> fn)
{
	return stack.scope(ns, fn);
}

void Runtime::scope(std::vector<Variable> & variables, std::vector<Ref<Value>> values, std::function<void(Ref<Runtime>)> fn)
{
	Namespace ns{ variables, values };
	scope(ns, fn);
}

bool Runtime::scope(Ref<Value> & result, std::function<bool(Ref<Value>&)> fn)
{
	Namespace ns{};
	return scope(ns, result, fn);
}

bool Runtime::scope(Namespace & ns, Ref<Value> & result, std::function<bool(Ref<Value>&)> fn)
{
	bool returned = false;
	scope(ns, [&returned, &fn, &result](Ref<Runtime> runtime) { returned = fn(result); });
	return returned;
}

bool Runtime::scope(std::vector<Variable> & variables, std::vector<Ref<Value>> values, Ref<Value> & result, std::function<bool(Ref<Value>&)> fn)
{
	Namespace ns{ variables, values };
	return scope(ns, result, fn);
}

Ref<Void> Omni::Runtime::void_() const
{
	return std::make_shared<Void>(rootType);
}

Ref<Number> Runtime::number(double d) const
{
	return std::make_shared<Number>(numberType, d);
}

Ref<List> Runtime::list() const
{
	return std::make_shared<List>(listType);
}

Ref<List> Runtime::list(std::vector<Ref<Value>> v) const
{
	return std::make_shared<List>(listType, v);
}

Ref<Object> Runtime::object(Ref<Class> c) const
{
	return std::make_shared<Object>(c);
}

Ref<SimpleObject> Runtime::object(Ref<Interface> i) const
{
	return std::make_shared<SimpleObject>(objectType, i);
}

Ref<Interface> Runtime::interface(std::initializer_list<std::pair<Ref<Type>, std::string>> pairs) const
{
	auto result = std::make_shared<Interface>(interfaceType);
	result->init(objectType, pairs);
	return result;
}

void Runtime::export(std::string name)
{
	exportedSymbols.insert(name);
}
Ref<Value> Runtime::meta(Ref<List> scope)
{
	return execute(scope)->back();
}
void Runtime::mesa(Ref<List> scope)
{

}

Ref<Runtime> Runtime::getMetaRuntime()
{
	if (metaRuntime == nullptr)
	{
		metaRuntime = std::make_shared<Runtime>();
	}
	return metaRuntime;
}

Ref<Runtime> Runtime::getMesaRuntime()
{
	if (mesaRuntime == nullptr)
	{
		mesaRuntime = std::make_shared<Runtime>();
	}
	return mesaRuntime;
}