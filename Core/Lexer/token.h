#ifndef TOKEN_H
#define TOKEN_H

#include <string>
#include <regex>
#include <functional>
#include <memory>

#include "../Types/value.h"

namespace Omni
{
	class Lexeme;
	class Pattern;
	class IRule;
	class IPattern;

	class Token : public std::enable_shared_from_this<Token>
	{
	public:
		using Match = std::match_results<std::string::const_iterator>;

		std::string name;
		std::regex pattern;

		Token(std::string name, std::string pattern);

		bool lex(std::string text, Lexeme & result);

		Token & operator=(Token &);
	};
}

#endif // TOKEN_H
