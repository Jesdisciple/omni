#ifndef LEXEME_H
#define LEXEME_H

#include <string>

#include "../Common/ref.h"

#include "token.h"

namespace Omni
{
	class Lexeme
	{
	private:
		friend class Lexer;
		Lexeme();

		Ref<Token> token;
		std::string text;
		size_t length;

	public:
		Lexeme(Ref<Token> token, std::string text, size_t length);
		virtual ~Lexeme();
		
		Lexeme & operator=(Lexeme &);

		Ref<Token> getToken();
		std::string getText() const;
		int getLength();
	};
}

#endif // LEXEME_H
