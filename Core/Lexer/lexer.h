#ifndef LEXER_H
#define LEXER_H

#include <string>
#include <regex>
#include <functional>
#include <vector>
#include <map>

#include "../Common/ref.h"
#include "../Types/value.h"
#include "token.h"
#include "lexeme.h"

namespace Omni
{
	class Lexer
	{
	private:
		std::map<std::string, Ref<Token>> tokens;

	public:
		Lexer();

		Ref<Token> defineToken(std::string name, std::string pattern);

		Ref<Token> getToken(std::string name);
		std::vector<Ref<Token>> getTokens();

		std::vector<Lexeme> scan(std::string text);

		std::map<std::string, Ref<Token>>::const_iterator cbegin();
		std::map<std::string, Ref<Token>>::const_iterator cend();
	};
}

#endif // LEXER_H
