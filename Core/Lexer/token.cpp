#include "token.h"

#include "lexeme.h"

using namespace Omni;

Token::Token(std::string name, std::string pattern)
	: name(name), pattern(pattern)
{
}

bool Token::lex(std::string text, Lexeme & result)
{
	Match m;
	if (std::regex_search(text, m, this->pattern))
	{
		std::string symbol = m[1];
		result = Lexeme(shared_from_this(), symbol, m.length());
		return true;
	}

	return false;
}

Token & Token::operator=(Token & other)
{
	name = other.name;
	pattern.assign(other.pattern);

	return *this;
}
