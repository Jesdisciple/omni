#include "lexeme.h"

#include "token.h"

using namespace Omni;

Lexeme::Lexeme()
	: token(), text(""), length(0)
{
}

Lexeme::Lexeme(Ref<Token> token, std::string text, size_t length)
	: token(token), text(text), length(length)
{
}

Lexeme::~Lexeme()
{
}

Lexeme & Lexeme::operator=(Lexeme & other)
{
	token = other.token;
	text = other.text;
	length = other.length;

	return *this;
}

Ref<Token> Lexeme::getToken()
{
	return token;
}
std::string Lexeme::getText() const
{
	return text;
}
int Lexeme::getLength()
{
	return length;
}

