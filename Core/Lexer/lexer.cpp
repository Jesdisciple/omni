#include <exception>

#include "lexer.h"

using namespace Omni;

Lexer::Lexer()
{
}

Ref<Token> Lexer::defineToken(std::string name, std::string pattern)
{
	if (this->tokens.find(name) != this->tokens.end())
	{ return nullptr; }

	auto result = std::make_shared<Token>(name, "^" + pattern);
	this->tokens[name] = result;
	return result;
}

Ref<Token> Lexer::getToken(std::string name)
{
	auto it = this->tokens.find(name);
	if (it == this->tokens.end())
	{ return nullptr; }

	return it->second;
}

std::vector<Ref<Token>> Lexer::getTokens()
{
	using map_pair = decltype(tokens)::value_type;
	std::vector<Ref<Token>> result;
	std::transform(tokens.begin(), tokens.end(), std::back_inserter(result), [](map_pair & pair){ return pair.second; });
	return result;
}

std::vector<Lexeme> Lexer::scan(std::string text)
{
	std::vector<Lexeme> result;

	size_t i = 0;
	while (i < text.length())
	{
		for (auto pair : this->tokens)
		{
			auto name = pair.first;
			auto token = pair.second;

			Lexeme lexeme;
			if (token->lex(text.substr(i), lexeme))
			{
				i = i + lexeme.length;

				result.push_back(lexeme);
			}
		}
	}

	return result;
}

std::map<std::string, Ref<Token>>::const_iterator Lexer::cbegin()
{
	return tokens.cbegin();
}

std::map<std::string, Ref<Token>>::const_iterator Lexer::cend()
{
	return tokens.cend();
}
