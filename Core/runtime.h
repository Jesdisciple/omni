#ifndef RUNTIME_H
#define RUNTIME_H

#include <memory>
#include <set>
#include <string>

#include "Types/class.h"
#include "Types/type.h"
#include "Types/void.h"
#include "Types/list.h"
#include "Functions/functiontype.h"
#include "Types/number.h"
#include "Types/simpleobject.h"
#include "Types/primitivetype.h"

#include "Parser/identifier.h"

#include "Common/namespace.h"

#include "scopestack.h"

namespace Omni
{
	class Runtime
	{
	private:
		Ref<PrimitiveType> rootType;
		Ref<PrimitiveType> voidType;
		Ref<PrimitiveType> numberType;
		Ref<PrimitiveType> objectType;
		Ref<PrimitiveType> classType;
		Ref<PrimitiveType> interfaceType;

		Ref<PrimitiveType> listType;
		Ref<PrimitiveType> stringType;

		ScopeStack metaStack;
		ScopeStack stack;

		Ref<Runtime> metaRuntime, mesaRuntime;

		std::set<std::string> exportedSymbols;

	public:
		Runtime();
		~Runtime();

		Runtime & operator =(Runtime & other);

		Ref<PrimitiveType> getTypeType() const;
		Ref<PrimitiveType> getVoidType() const;
		Ref<PrimitiveType> getNumberType() const;
		Ref<PrimitiveType> getListType() const;
		Ref<PrimitiveType> getObjectType() const;
		Ref<PrimitiveType> getClassType() const;
		Ref<PrimitiveType> getInterfaceType() const;

		Ref<FunctionType> getFunctionType(Ref<Type> returnType, std::string name, std::initializer_list<Ref<Type>> paramTypes);

		Ref<Class> defineClass(std::string name, Ref<Interface> i);

		void declareVariable(Ref<Type> type, std::string name);
		void declareVariable(Ref<Type> type, std::string name, Ref<Value> value);

		void assignVariable(std::string name, Ref<Value> value);

		Ref<Value> resolveVariable(std::string name, bool exportedOnly = false);
		Ref<Value> resolveVariable(Ref<Identifier> name, bool exportedOnly = false);
		bool tryResolveVariable(std::string name, Ref<Value> & out_value, bool exportedOnly = false);
		bool tryResolveVariable(Ref<Identifier> name, Ref<Value> & out_value, bool exportedOnly = false);

		Ref<List> preprocess(Ref<List> original);
		Ref<List> compile(Ref<List> original);
		Ref<List> execute(Ref<List> original);
		Ref<Value> resolve(Ref<Value> original);

		Ref<Type> getTypeOfVariable(std::string name);

		void scope(std::function<void(Ref<Runtime>)> fn);
		void scope(Namespace & ns, std::function<void(Ref<Runtime>)> fn);
		void scope(std::vector<Variable> & variables, std::vector<Ref<Value>> values, std::function<void(Ref<Runtime>)> fn);

		bool scope(Ref<Value> & result, std::function<bool(Ref<Value>&)> fn);
		bool scope(Namespace & ns, Ref<Value> & result, std::function<bool(Ref<Value>&)> fn);
		bool scope(std::vector<Variable> & variables, std::vector<Ref<Value>> values, Ref<Value> & result, std::function<bool(Ref<Value>&)> fn);

		Ref<Void> void_() const;
		Ref<Number> number(double d) const;
		Ref<List> list() const;
		Ref<List> list(std::vector<Ref<Value>> v) const;
		Ref<Object> object(Ref<Class> c) const;
		Ref<SimpleObject> object(Ref<Interface> i) const;
		Ref<Interface> interface(std::initializer_list<std::pair<Ref<Type>, std::string>> pairs) const;

		void export(std::string name);
		Ref<Value> meta(Ref<List> scope);
		void mesa(Ref<List> scope);

		Ref<Runtime> getMetaRuntime();
		Ref<Runtime> getMesaRuntime();

		template<typename T, typename ... Args>
		Ref<T> make(Args ... args)
		{
			T * raw = new T(args...);
			Ref<Value> ref = Ref<Value>(raw);
			return std::static_pointer_cast<T>(ref);
		}

		friend class Runtime;
	};
}

#endif // RUNTIME_H
