#-------------------------------------------------
#
# Project created by QtCreator 2014-11-19T15:04:35
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Omni
CONFIG   += console
CONFIG   -= qt app_bundle

QMAKE_CXXFLAGS += -std=c++11 #-fsanitize=address

TEMPLATE = app

SOURCES += main.cpp \
		Types/object.cpp \
		Types/number.cpp \
		Lexer/lexer.cpp \
		Types/class.cpp \
		Lexer/token.cpp \
		Lexer/lexeme.cpp \
		Types/value.cpp \
		Types/pointertype.cpp \
		Types/pointer.cpp \
		Types/simpleobject.cpp \
		runtime.cpp \
		Common/namespace.cpp \
		Common/variable.cpp \
		scopestack.cpp \
		Types/type.cpp \
		Types/primitivetype.cpp \
		Types/Generics/genericclass.cpp \
		Types/Generics/typeparameter.cpp \
		Types/interface.cpp \
    Functions/functiontype.cpp \
    Common/ref.cpp

HEADERS += \
		Types/object.h \
		Types/number.h \
		Lexer/lexer.h \
		Types/class.h \
		Lexer/token.h \
		Lexer/lexeme.h \
		Types/value.h \
		Types/pointertype.h \
		Types/pointer.h \
		Types/simpleobject.h \
		runtime.h \
                Common/namespace.h \
                Common/variable.h \
		scopestack.h \
		Types/type.h \
		Types/primitivetype.h \
		Types/Generics/genericclass.h \
		Types/Generics/typeparameter.h \
		Types/interface.h \
                Functions/functiontype.h \
                Common/ref.h
