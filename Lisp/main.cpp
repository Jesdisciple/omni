#include <iostream>
#include <exception>
#include <string>
#include <queue>
#include <sstream>
#include <stack>
#include <vector>

#include "Core/Types/class.h"
#include "Core/Types/type.h"
#include "Core/Types/void.h"
#include "Core/Types/number.h"
#include "Core/Types/list.h"
#include "Core/Types/simpleobject.h"
#include "Core/Types/primitivetype.h"

#include "Core/runtime.h"

#include "Core/Functions/functiontype.h"
#include "Core/Functions/function.h"

#include "Core/Lexer/token.h"
#include "Core/Lexer/lexer.h"

#include "Core/Parser/parser.h"
#include "Core/Parser/identifier.h"

#include "Core/Util/debug.h"
#include "Core/Util/strings.h"

#include "Core/lisp.h"

using namespace Omni;
using namespace Omni::Util;

int main(int argc, char *argv[])
{
	try
	{
		Lisp lisp;
		auto output = lisp.eval("(+ (+ 4 3) (+ 2 1))");
		auto first = output->front();
		auto list = first->as<List>();

		if (list == nullptr)
		{
			std::cout << first << std::endl;
		}
		else
		{
			for (auto value : *list)
			{
				std::cout << value << std::endl;
			}
		}
	}
	catch (std::exception & e)
	{
		std::cerr << "LAST-CHANCE EXCEPTION: " << e.what();
	}

	std::cout << "Press Any key to continue.";
	std::cout.flush();
	std::cin.ignore();
}

void morse()
{
	Lexer lexer;
	lexer.defineToken("dot", "(\\.)(?!\\.)");
	lexer.defineToken("dash", "(\\.\\.\\.)(?!\\.)");
	lexer.defineToken("word-gap", "(       )(?=\\.)");
	lexer.defineToken("letter-gap", "(   )(?=\\.)");
	lexer.defineToken("element-gap", "( )(?=\\.)");

	auto lex = lexer.scan(". . . ... ... ... . . .");
	for (Lexeme l : lex)
	{
		std::cout << l.getText() << l.getLength() << std::endl;
	}
}