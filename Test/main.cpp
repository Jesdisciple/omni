#include <iostream>
#include <exception>
#include <string>
#include <queue>
#include <sstream>

#include "Core\Types\class.h"
#include "Core\Types\type.h"
#include "Core\Types\number.h"
#include "Core\Types\simpleobject.h"
#include "Core\Types\primitivetype.h"

#include "Core\runtime.h"

#include "Core\Lexer\token.h"
#include "Core\Lexer\lexer.h"

#include "Core\Parser\rule.h"
#include "Core\Parser\parser.h"
#include "Core\Parser\pattern.h"

#include "Core\Util\debug.h"
#include "Core\Util\strings.h"

#include "Core\Parser\parseleaf.h"
#include "Core\Parser\parsebranch.h"
#include "Core\Parser\parsetwig.h"

#include <gtest\gtest.h>

using namespace Omni;
using namespace Omni::Util;

Ref<Value> plus(std::vector<std::function<Ref<Value>()>> v)
{
	if (v.size() != 2)
	{
		//TODO: exception
	}

	Ref<Number> a, b;
	a = std::static_pointer_cast<Number>(v[0]());
	b = std::static_pointer_cast<Number>(v[1]());
	return Number::create(a->getType(), a->getValue() + b->getValue());
}

TEST(LexerTest, Tokenization)
{
	Lexer lexer;

	lexer.defineToken("a", "a");
	lexer.defineToken("b", "b");
	lexer.defineToken("c", "c");

	std::vector<Lexeme> lex = lexer.scan("abcbcacba");

	ASSERT_EQ(9, lex.size());
}

TEST(LexerTest, TokenizationFail)
{
	Lexer lexer;

	lexer.defineToken("a", "a");
	lexer.defineToken("b", "b");
	lexer.defineToken("c", "c");

	std::vector<Lexeme> lex = lexer.scan("abcbfcacba");

	ASSERT_EQ(9, lex.size());
}

int main(int argc, char *argv[])
{
	try
	{
		PRINT("Testing lexer.");

		Lexer lexer;

		lexer.defineToken("number", "\\s*([-+]?\\d+(?:\\.\\d+)?)\\s*");
		lexer.defineToken("plus", "\\s*(\\+)\\s*");
		lexer.defineToken("minus", "\\s*(\\-)\\s*");
		lexer.defineToken("lparen", "\\s*(\\()\\s*");
		lexer.defineToken("rparen", "\\s*(\\))\\s*");

		auto lex = lexer.scan("4 + 3 + (2 + 1)");
		for (Lexeme l : lex)
		{
			std::cout << l.getToken()->name << " " << l.getText() << " " << l.getLength() << " " << std::endl;
		}

		PRINT("Lexer tests complete.");

		PRINT("Testing parser.");

		Parser parser{ lexer };

		Runtime runtime;

		parser.defineRule("number", [&runtime](Lexeme & lexeme, Ref<const IParseNode> & result)
		{
			auto value = Number::create(runtime.getNumberType(), std::stof(lexeme.getText()));
			result = std::make_shared<ParseLeaf>(lexeme, value);
			return true;
		});

		parser.defineRule("plus", [&runtime](Lexeme & lexeme, Ref<const IParseNode> & result)
		{
			if (trim(lexeme.getText()) != "+")
			{
				return false;
			}

			result = std::make_shared<ParseTwig>(lexeme, [](Ref<ParseBranch> & parent)
			{
				parent->setOperation(plus);
			});
			return true;
		});

		parser.defineRule("operator1",
			parser.getRule("minus") | parser.getRule("plus"));

		parser.defineRule("parens",
			parser.getRule("lparen") +
			(parser.getRule("operation1") | parser.getRule("number") | parser.getRule("parens")) +
			parser.getRule("rparen"),
			[](std::vector<Ref<const IParseNode>> v, int precedence)
		{
			return ParseBranch::create(precedence, v[1]->flatten());
		});

		parser.defineRule("expr",
			parser.getRule("number") | parser.getRule("parens"));

		parser.defineRule("operation1", 1,
			parser.getRule("expr") + parser.getRule("operator1") +
			(parser.getRule("expr") | parser.getRule("operation1")));

		auto parseTree = parser.parse("4 + 3 + (2 + 1)");

		std::vector<size_t> path;
		parseTree->iterate(path, [](std::vector<size_t> path, Ref<const IParseNode> node)
		{
			for (size_t i = 0; i < path.size(); i++)
			{
				if (i != 0) { std::cout << "."; }
				std::cout << path[i];
			}
			std::cout << ": ";
			node->writeTo(std::cout);
			std::cout << std::endl;
		});

		Ref<Number> result = std::static_pointer_cast<Number>(parseTree->evaluate());

		PRINT(" = " << result->getValue());
		if (result->getValue() == 4 + 3 + (2 + 1))
		{
			PRINT("SUCCESS");
		}
		else
		{
			PRINT("FAILED");
		}

		PRINT("Parser tests complete.");

		PRINT("Testing runtime.");

		std::cout << "qux\n";
		std::cout.flush();
		runtime.scope([&runtime]()
		{
			Ref<Interface> IFoo = runtime.interface(
			{
				{ runtime.getNumberType(), "bar" },
			});

			Ref<Class> Foo = runtime.defineClass("Foo", IFoo);

			Ref<Object> foo = runtime.object(Foo);
			std::cout << "object type: " << *foo->getType() << "\n";
			foo->assignVariable("bar", runtime.number(42));
			//foo->assignVariable("bar", foo);
		});

		PRINT("Runtime tests complete.");
	}
	catch (std::exception & e)
	{
		std::cerr << "LAST-CHANCE EXCEPTION: " << e.what();
	}

	std::cout << "Press Any key to continue.";
	std::cout.flush();
	std::cin.ignore();
}

void morse()
{
	Lexer lexer;
	lexer.defineToken("dot", "(\\.)(?!\\.)");
	lexer.defineToken("dash", "(\\.\\.\\.)(?!\\.)");
	lexer.defineToken("word-gap", "(       )(?=\\.)");
	lexer.defineToken("letter-gap", "(   )(?=\\.)");
	lexer.defineToken("element-gap", "( )(?=\\.)");

	auto lex = lexer.scan(". . . ... ... ... . . .");
	for (Lexeme l : lex)
	{
		std::cout << l.getText() << l.getLength() << std::endl;
	}
}