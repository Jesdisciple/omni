#ifndef REF_H
#define REF_H

#include <memory>
#include <ostream>

namespace Omni
{
	template<typename T>
	using Ref = std::shared_ptr<T>;
}

template<typename T>
std::ostream & operator<<(std::ostream & s, const Omni::Ref<T> value)
{
	s << *value;
	return s;
}

#endif // REF_H
